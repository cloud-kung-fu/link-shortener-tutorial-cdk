import {
  CkfRestApiEndpointConfig,
  CkfRestApiConfig,
  LambdaLayerConfig
} from '@cloudkungfu/ckf-cdk-rest-api/dist/lib';
import {Runtime} from '@aws-cdk/aws-lambda';

export const ckfToolkitLayerConfig: LambdaLayerConfig = {
  description: 'CKF API Toolkit Library',
  id: 'CkfToolkitLayer',
  runtimes: [Runtime.PYTHON_3_7],
  sourcePath: 'layers/ckf_toolkit_layer',
};

export const getActualUrlEndpoint: CkfRestApiEndpointConfig = {
  apiPath: "links/{id}",
  function: {
    sourcePath: '../link-shortener-tutorial',
    handlerFile: 'handler',
    handlerFunction: 'get_actual_url_from_short',
    runtime: Runtime.PYTHON_3_7,
    layers: [ckfToolkitLayerConfig.id]
  },
  httpMethod: 'GET',
  id: "getActualUrlEndpoint",
}

export const createLinkEndpoint: CkfRestApiEndpointConfig = {
  apiPath: "links",
  function: {
    sourcePath: '../link-shortener-tutorial',
    handlerFile: 'handler',
    handlerFunction: 'create_link',
    runtime: Runtime.PYTHON_3_7,
    layers: [ckfToolkitLayerConfig.id]
  },
  httpMethod: 'POST',
  id: "createLinkEndpoint",
}

export const updateLinkEndpoint: CkfRestApiEndpointConfig = {
  apiPath: "links/{id}",
  function: {
    sourcePath: '../link-shortener-tutorial',
    handlerFile: 'handler',
    handlerFunction: 'update_link',
    runtime: Runtime.PYTHON_3_7,
    layers: [ckfToolkitLayerConfig.id]
  },
  httpMethod: 'PUT',
  id: "updateLinkEndpoint",
}

export const getTotalClicksForLinkEndpoint: CkfRestApiEndpointConfig = {
  apiPath: "links/{id}/total_clicks",
  function: {
    sourcePath: '../link-shortener-tutorial',
    handlerFile: 'handler',
    handlerFunction: 'get_total_clicks_for_link',
    runtime: Runtime.PYTHON_3_7,
    layers: [ckfToolkitLayerConfig.id]
  },
  httpMethod: 'GET',
  id: "getTotalClicksForLinkEndpoint",
}

export const getTotalClicksPerDayForLinkEndpoint: CkfRestApiEndpointConfig = {
  apiPath: "links/{id}/total_clicks/{date}",
  function: {
    sourcePath: '../link-shortener-tutorial',
    handlerFile: 'handler',
    handlerFunction: 'get_total_clicks_for_link_by_day',
    runtime: Runtime.PYTHON_3_7,
    layers: [ckfToolkitLayerConfig.id]
  },
  httpMethod: 'GET',
  id: "getTotalClicksPerDayForLinkEndpoint",
}

export const getTotalClicksByPersonForLinkEndpoint: CkfRestApiEndpointConfig = {
  apiPath: "links/{id}/total_clicks_person/{ip}",
  function: {
    sourcePath: '../link-shortener-tutorial',
    handlerFile: 'handler',
    handlerFunction: 'get_total_clicks_for_link_by_person',
    runtime: Runtime.PYTHON_3_7,
    layers: [ckfToolkitLayerConfig.id]
  },
  httpMethod: 'GET',
  id: "getTotalClicksByPersonForLinkEndpoint",
}

export const getTotalClicksByPersonForLinkByDayEndpoint: CkfRestApiEndpointConfig = {
  apiPath: "links/{id}/total_clicks_person/{ip}/date/{date}",
  function: {
    sourcePath: '../link-shortener-tutorial',
    handlerFile: 'handler',
    handlerFunction: 'get_total_clicks_for_link_by_person_by_day',
    runtime: Runtime.PYTHON_3_7,
    layers: [ckfToolkitLayerConfig.id]
  },
  httpMethod: 'GET',
  id: "getTotalClicksByPersonForLinkByDayEndpoint",
}

export const apiConfig: CkfRestApiConfig = {
  authorizers: [],
  cognitoPools: [],
  endpoints: [getActualUrlEndpoint, createLinkEndpoint, updateLinkEndpoint,
    getTotalClicksForLinkEndpoint, getTotalClicksPerDayForLinkEndpoint, getTotalClicksByPersonForLinkEndpoint,
    getTotalClicksByPersonForLinkByDayEndpoint],
  layers: [ckfToolkitLayerConfig]
}
