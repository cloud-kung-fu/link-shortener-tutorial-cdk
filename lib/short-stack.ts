import * as cdk from '@aws-cdk/core';
import {AttributeType, BillingMode, Table} from '@aws-cdk/aws-dynamodb';
import {TableAttributes} from '../constants/table-attributes'
import {CkfRestApi} from '@cloudkungfu/ckf-cdk-rest-api';
import {apiConfig} from "./api-config";

export interface ShortStackProps extends cdk.StackProps {
  numberOfGSIs: number;
  tableName: string;
}

export class ShortStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: ShortStackProps) {
    super(scope, id, props);

    const shortLinksApi = new CkfRestApi(this, 'ShortenerCkfApi', {
      stage: 'dev',
      id: 'ShortenerApi',
      name: 'A link shortener API',
      config: apiConfig,
    })

    const shortLinksTable = new Table(this, 'ShortLinksTable', {
      tableName: props.tableName,
      billingMode: BillingMode.PAY_PER_REQUEST,
      partitionKey: {
        name: TableAttributes.PK,
        type: AttributeType.STRING
      },
      sortKey: {
        name: TableAttributes.SK,
        type: AttributeType.STRING
      }
    })
    Object.values(shortLinksApi.endpointLambdaFunctions).forEach((endpointLambdaFunction) => {
      shortLinksTable.grantReadWriteData(endpointLambdaFunction.function);
    });

    const numberOfGSIsRange = Array(props.numberOfGSIs)
      .fill(null)
      .map((_, i) => i + 1);

    numberOfGSIsRange.forEach((number) => {
      shortLinksTable.addGlobalSecondaryIndex({
        indexName: `GSI_${number}`,
        partitionKey: {
          name: [TableAttributes.GSI_PREFIX, number, TableAttributes.PK].join(TableAttributes.DELIMITER),
          type: AttributeType.STRING,
        },
        sortKey: {
          name: [TableAttributes.GSI_PREFIX, number, TableAttributes.SK].join(TableAttributes.DELIMITER),
          type: AttributeType.STRING,
        },
      });
    });
  }
}
