#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import {ShortStack, ShortStackProps} from "../lib/short-stack";

const app = new cdk.App();
const props: ShortStackProps = {
  numberOfGSIs: 2,
  tableName: "ShortLinkTable"
}
new ShortStack(app, 'ShortStack', props);
