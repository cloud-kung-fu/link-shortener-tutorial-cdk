export enum TableAttributes {
  PK = 'PK',
  SK = 'SK',
  GSI_PREFIX = 'GSI',
  DELIMITER = '_',
}
