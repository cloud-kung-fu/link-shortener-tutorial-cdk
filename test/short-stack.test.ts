import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as Short from '../lib/short-stack';

test('Empty Stack', () => {
    const app = new cdk.App();
    const props: Short.ShortStackProps = {
      numberOfGSIs: 2,
      tableName: "ShortLinksTable"
    }
    // WHEN
    const stack = new Short.ShortStack(app, 'MyTestStack', props);
    // THEN
    expectCDK(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});
